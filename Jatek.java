﻿import java.util.*;
import java.io.*;

public class Jatek {
	private ArrayList<Kerdes> kerdesek;
	private int hely;
	private int bazis;
	private boolean kozonseg, felezo, telefon;

	/**
	 * Konstruktor beallitja a kezdoertekeket, es felteszi az elso kerdest.
	 */
	public Jatek() {
		hely = 0;
		kerdesek = Adat.konnyu;
		kozonseg = felezo = telefon = true;
		kerdez();
	}

	/**
	 * Kiirja a jelenlegi allast majd megkerdezi hogy tovabb szeretne-e haladni
	 * a jatekos. Ha igen akkor meghivja a kerdez() eljarast, ha nem akkor
	 * meghivja a gameover eljarast igaz ertekkel.
	 */
	public void menu() {
		System.out.println("Osszegek:");
		for (int i = 1; i < Adat.osszegek.length; i++) {
			System.out.println(i + ". " + Adat.osszegek[i]);
		}

		System.out.println("Jelenlegi osszeg: " + Adat.osszegek[hely]);
		System.out.println("Garantalt osszeg: " + Adat.osszegek[bazis]);
		String parancs = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (!parancs.equals("next") && !parancs.equals("stop")) {
			System.out
					.println("Ha szeretne tovabbhaladni irja be a 'next' parancsot, ha meg szeretne allni irja be a 'stop' parancsot");
			try {
				parancs = br.readLine();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		if (parancs.equals("next")) {
			kerdez();
		} else {
			gameover(true);
		}
	}

	/**
	 * Kivalasztja a kovetkezo kerdest es meghivja az adott peldany kerdez()
	 * eljarasat. Ha jol valaszol a jatekos a menube dob ha hamis akkor a
	 * gameoverbe.
	 */
	public void kerdez() {
		Random rand = new Random();
		Kerdes akt;
		int n = rand.nextInt(kerdesek.size());
		akt = kerdesek.get(n);
		kerdesek.remove(n);
		if (akt.kerdez(this)) {
			hely++;
			bazisset();
			menu();
		} else {
			gameover(false);
		}
	}

	/**
	 * Megvizsgalja a jatekos helyet hogy bazisertek-e. Valamint a megfelelo
	 * helyeken atallitja a hasznalt kerdes listat.
	 */
	public void bazisset() {
		if (hely >= 11) {
			bazis = 11;
			kerdesek = Adat.nehez;
		} else if (hely >= 7) {
			bazis = 7;
			kerdesek = Adat.kozepes;
		} else if (hely >= 3) {
			bazis = 3;
		} else {
			bazis = 0;
		}
	}

	/**
	 * A jatek befejezese. Megvizsgálja hogy a jatekos megallt-e es aszerint
	 * odaiteli a helyezese vagy a bazisa osszeget.
	 * 
	 * @param megallt
	 */
	public void gameover(boolean megallt) {
		if (megallt) {
			System.out.println("Gratulalok a nyeremenye: "
					+ Adat.osszegek[hely]);
		} else {
			System.out.println("Gratulalok a nyeremenye: "
					+ Adat.osszegek[bazis]);
		}
	}

	public boolean isKozonseg() {
		return kozonseg;
	}

	public void setKozonseg(boolean kozonseg) {
		this.kozonseg = kozonseg;
	}

	public boolean isFelezo() {
		return felezo;
	}

	public void setFelezo(boolean felezo) {
		this.felezo = felezo;
	}

	public boolean isTelefon() {
		return telefon;
	}

	public void setTelefon(boolean telefon) {
		this.telefon = telefon;
	}

}
