﻿public abstract class Kerdes {
	protected String kerdes;

	/**
	 * Felteszi a kerdest az adott jatekban. Visszateresi erteke igaz, ha a
	 * felhasznalo helyesen valaszol, hamis ha nem.
	 * 
	 * @param meccs
	 * @return
	 */
	public abstract boolean kerdez(Jatek meccs);

	/**
	 * Parancsot ker a felhasznalotol. Protected mivel csak a peldanyon belul
	 * hasznaljuk.
	 * 
	 * @return
	 */
	protected abstract String beker();

	/**
	 * Megfelezi a valaszlehetosegeket.Protected mivel csak a peldanyon belul
	 * hasznaljuk.
	 * 
	 */
	protected abstract void felez();

	/**
	 * A kozonseg segítseget keri. Protected mivel csak a peldanyon belul
	 * hasznaljuk.
	 *
	 */
	protected abstract void kozonseg();

	/**
	 * Telefonos kerdes. Elrejti a helytelen megoldasokat.Protected mivel csak a
	 * peldanyon belul hasznaljuk.
	 * 
	 */
	protected abstract void telefon();

	public String getKerdes() {
		return kerdes;
	}

	public void setKerdes(String kerdes) {
		this.kerdes = kerdes;
	}

	/**
	 * Konstruktor beallítja a kerdes erteket.
	 * 
	 * @param kerdes
	 */
	public Kerdes(String kerdes) {
		this.kerdes = kerdes;
	}
} 
