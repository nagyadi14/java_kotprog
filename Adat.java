﻿import java.io.*;
import java.util.*;

public class Adat {
	private static final String path = "kerdesek.txt";
	static int[] osszegek;
	static BufferedReader br;
	static ArrayList<Kerdes> konnyu;
	static ArrayList<Kerdes> kozepes;
	static ArrayList<Kerdes> nehez;

	/**
	 * Beolvassa a kerdeseket majd kerdes peldanyokat hoz letre.
	 */
	public static void beolvas() {
		String kerdes;
		StringTokenizer st;
		konnyu = new ArrayList<Kerdes>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			while (!(kerdes = br.readLine()).equals("---")) {//konnyu kerdesek felvetele
				st = new StringTokenizer(kerdes, ";");
				if (st.nextToken().equals("IH")) {
					konnyu.add(new IKerdes(st.nextToken(), Integer.parseInt(st
							.nextToken())));
				} else {
					konnyu.add(new FKerdes(st.nextToken(), "A: "
							+ st.nextToken(), "B: " + st.nextToken(), "C: "
							+ st.nextToken(), "D: " + st.nextToken(), Integer
							.parseInt(st.nextToken())));
				}
			}
			kozepes = new ArrayList<Kerdes>();//kozepes kerdesek felvetele
			while (!(kerdes = br.readLine()).equals("---")) {
				st = new StringTokenizer(kerdes, ";");
				if (st.nextToken().equals("IH")) {
					kozepes.add(new IKerdes(st.nextToken(), Integer.parseInt(st
							.nextToken())));
				} else {
					kozepes.add(new FKerdes(st.nextToken(), "A: "
							+ st.nextToken(), "B: " + st.nextToken(), "C: "
							+ st.nextToken(), "D: " + st.nextToken(), Integer
							.parseInt(st.nextToken())));
				}
			} 
			nehez = new ArrayList<Kerdes>();//nehez kerdesek felvetele
			while (!(kerdes = br.readLine()).equals("---")) {
				st = new StringTokenizer(kerdes, ";");
				if (st.nextToken().equals("IH")) {
					nehez.add(new IKerdes(st.nextToken(), Integer.parseInt(st
							.nextToken())));
				} else {
					nehez.add(new FKerdes(st.nextToken(), "A: "
							+ st.nextToken(), "B: " + st.nextToken(), "C: "
							+ st.nextToken(), "D: " + st.nextToken(), Integer
							.parseInt(st.nextToken())));
				}
			} 
			br.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	/**
	 * Feltolti az osszegeket, melyeket a jatekos megnyerhet.
	 */
	public static void osszegek() {
		osszegek = new int[16];
		osszegek[0] = 0;
		osszegek[1] = 5000;
		osszegek[2] = 10000;
		osszegek[3] = 25000;
		osszegek[4] = 50000;// bazis
		osszegek[5] = 100000;
		osszegek[6] = 200000;
		osszegek[7] = 300000;
		osszegek[8] = 500000;// bazis
		osszegek[9] = 800000;
		osszegek[10] = 1500000;
		osszegek[11] = 3000000;
		osszegek[12] = 5000000;// bazis
		osszegek[13] = 10000000;
		osszegek[14] = 20000000;
		osszegek[15] = 40000000;
	}
}
